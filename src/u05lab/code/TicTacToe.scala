package u05lab.code

import u05lab.code.TicTacToe.Mark

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X() => O(); case _ => X()}
    override def toString: String = this match {case X() => "X"; case _ => "O"}
  }
  case class X() extends Player
  case class O() extends Player

  private val size = 3
  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board match {
    case Nil => None
    case Mark(`x`, `y`, p) :: t => Some(p)
    case _ :: t => find(t, x, y)
  }

  def canPlay(board: Board, x: Int, y: Int) = find(board, x, y).isEmpty

  def alternate(p: Player): Player = p match {
    case X() => O()
    case O() => X()
  }

  def placeAnyMark(board: Board, player: Player): Seq[Board] =
    for {
      x <- 0 until size
      y <- 0 until size
      newmark = Mark(x, y, player)
      if canPlay(board, x, y)
    } yield newmark :: board

  def isOver(board: Board): Boolean = {
    def innerIsOver(marks: Traversable[Mark]): Boolean = {
      def _checkMap(grouping: (Mark)=>Any): Boolean = marks.groupBy[Any](grouping).map(_._2.size).filter(_ == size).nonEmpty
      _checkMap(_.x) || _checkMap(_.y) || _checkMap(m => m.x - m.y) || _checkMap(m => m.x + m.y)
    }
    board.groupBy(_.player).map(_._2).find(innerIsOver(_)).nonEmpty
  }

  // Private because unsafe to use in general (missing check for empty games)
  private def nextMoves(g: Game, p: Player): Stream[Game] =
    if (isOver(g.head)) {
      Stream(g)
    } else {
      placeAnyMark(g.head, p).map(_ :: g).toStream
    }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
      case 0 => Stream(List[Board](List[Mark]()))
      case _ => computeAnyGame(alternate(player), moves - 1).flatMap(nextMoves(_, player))
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 until size; board <- game.reverse; x <- 0 until size) {
      print(find(board, x, y)
        .map(_.toString)
        .getOrElse("."))
      if (x == size - 1) {
        print(" ")
        if (board == game.head) {
          println()
        }
      }
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X())),0,0)) // Some(X)
  println(find(List(Mark(0,0,X()),Mark(0,1,O()),Mark(0,2,X())),0,1)) // Some(O)
  println(find(List(Mark(0,0,X()),Mark(0,1,O()),Mark(0,2,X())),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X()))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O())),X()))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O(), 7).foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!

}